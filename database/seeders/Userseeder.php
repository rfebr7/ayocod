<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('users')->insert(
            [
                [
                    'name' => 'Mas Admin',
                    'username' => 'admin',
                    'gender' => 1,
                    'email' => 'admin@gmail.com',
                    'password' => Hash::make('12345678'),
                    'role_id' => 1
                ],
                [
                    'name' => 'Reza Febriansyah',
                    'username' => 'iamr7',
                    'gender' => 1,
                    'email' => 'reza@gmail.com',
                    'password' => Hash::make('12345678'),
                    'role_id' => 2
                ],
                [
                    'name' => 'Ahmad Hizbullah Akbar',
                    'username' => 'ahmadhiz',
                    'gender' => 1,
                    'email' => 'akbar@gmail.com',
                    'password' => Hash::make('12345678'),
                    'role_id' => 2
                ],
            ]
        );
    }
}
